module.exports = {
  parser: '@babel/eslint-parser',
  extends: ['airbnb-base'],
  plugins: ['react'],
  parserOptions: {
    ecmaVersion: 2017,
    ecmaFeatures: {
      jsx: true
    }
  },
  rules: {
    strict: 0,
    semi: [2, 'always'],
    'no-console': 0,
    'no-underscore-dangle': 0,
    'no-unused-vars': ['error', { vars: 'all', args: 'after-used', ignoreRestSiblings: false, argsIgnorePattern: '^_' }],
    'react/jsx-uses-react': 'error',
    'react/jsx-uses-vars': 'error',
    'react/jsx-no-undef': [2],
    'func-names': ['warn', 'as-needed'],
  },
  env: {
    browser: true,
    mocha: true,
  },
  settings: {
    "import/resolver": {
      "node": {
        "extensions": [".js", ".jsx"],
      },
    },
  }
};
