# @teron/eslint-config-react

Default ESlint configuration for React projects.

## Installation

First install the core packages:

    npm install git+ssh://git@bitbucket.org/trelltech/eslint-react-config.git -D

Then the peer dependencies (as needed):

    npm install babel-eslint eslint eslint-config-airbnb-base \
        eslint-plugin-react eslint-plugin-import -D

## Configuration

Add the following extend directive to `.eslintrc.json`:

    {
      "extends": "@teron/eslint-config-react"
    }
